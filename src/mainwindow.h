#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QTableView>
#include <QStandardItemModel>
namespace Ui {
class MainWindow;
}

class FileData      //文件数据类, 用于保存一个文件的路径
{
public:
    QString pathSrc;      //保存获取到的文件路径       eg: F:/mysrc/1.txt
    QString pathTmp;      //保存文件的路径(除去文件名) eg: F:/mysrc
    QString pathDest;     //保存修改后的文件路径(最终通过该路径,重命名文件)  eg: F/mysrc/2.txt
    QString nameSrc;      //拆分path_src后获得的文件名 eg: 1
    QString nameDest;     //修改后的新文件名           eg: 2
    QString fileType;      //文件类型                   eg: txt
    FileData(QString &_path_src){pathSrc = _path_src;} //构造函数
};
class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    void create_fileList(QList<QString> &fileNameList); //根据选择的文件的文件名列表, 生成文件数据类列表
    void printList(const QList<QString> &list); //打印QList<QString>
    void printList(const QList<FileData> &list); //打印QList<FileData>
    void updateTable(void); //改变表格显示内容(原理上就是改变与表格绑定的 list_model)
    void tableInit(void);   //表格初始化显示
    void rowList_update(void);  //获取当前多选的表格项的行号, 排序去重后存于rowList中
    void nameDest_2_pathDest(void); //根据文件数据列表中的nameDest, 生成pathDest

private slots:
    void on_btn_addFile_clicked();

    void on_btn_deleteFile_clicked();

    void on_btn_startChange_clicked();

    void on_btn_detetRecord_clicked();

    void on_btn_makeNewName_clicked();

    void tabRowValueChanged(const QModelIndex &, const QModelIndex &); //表格行内容被修改

private:
    Ui::MainWindow *ui;

    QList<FileData> fileList;   //文件数据列表, 用于存储各文件的数据
    QList<int> rowList; //当前选择行号列表, 用于保存当前选择的行的行号
    QStandardItemModel *list_model;  //和oTableView绑定
};

#endif // MAINWINDOW_H
